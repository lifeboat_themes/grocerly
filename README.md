#Grocerly

## Home Page

### Banner
You can have up to 2 Collections and 2 Products to be shown in the banner, which is shown as soon as the website is open.

This is done by selecting either the collections or products, or both, in the custom fields labelled

**Banner Collection 1, Banner Collection 2, Banner Product 1, Banner Product 2**

_NOTE_: On the banners you can also have a custom image and also a custom subtitle to be shown in the banner.

These are done by going on the specific Product or Collection, scrolling down to the bottom of the page and select an image and/or type a subtitle in the fields labelled

#### Collection: 
Home Banner Image, Collection Subtitle

#### Product:
Banner Image, Product Subtitle

### Carousel Collections
You can have up to 4 collections to be featured in a tabbed carousel.
To select these collections, go to the custom fields labelled

**Carousel Collection 1, Carousel Collection 2, Carousel Collection 3, Carousel Collection 4**

and select which collections are to be shown in this section of the home page.

### Featured Collections

In this section you can have up to 3 collections to be featured together.

You can select the collections to be featured in the custom fields labelled

**Featured Collection 1, Featured Collection 2, Featured Collection 3**

### Display Collections
There are 2 custom fields to have display collections. These are shown separately. **_Display Collection 1_** is shown after the featured collections
and **_Display Collection 2_** is shown after the Parallax section.

To select which collections to display, there are two custom fields labelled as shown above.

### Parallax Collection
You have an option to select a Collection to be featured in a parallax banner. This is done by selecting a collection in the custom field
labelled **Parallax Collection**. On the banner, the collection's subtitle will also be shown. If that is not set, the summary will be shown instead, and if that is also empty,
the META description will be shown.

_NOTE:_ On the Parallax Banner, you can select a custom image to be shown instead of the collection image.
This is done by going to edit the selected collection, scrolling to the bottom and uploading an image to the
**Parallax Image** custom field.

## Footer
### Footer: First Menu
_Note: Submenus items will not be displayed_

### Footer: Second Menu
_Note: Submenus items will not be displayed_

