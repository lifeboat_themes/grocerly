<section class="products-wrapper container mt-10 pb-lg-4 mb-8">
    <div class="title-wrapper mb-4">
        <h2 class="title title-underline mb-0">$Collection.Title</h2>
        <a href="$Collection.AbsoluteLink" class="btn btn-link">View All<i
                class="d-icon-arrow-right"> </i></a>
    </div>
    <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                    'items': 4,
                    'nav': false,
                    'dots': false,
                    'margin': 20,
                    'loop': false,
                    'responsive': {
                        '0': {
                            'items': 2
                        },
                        '768': {
                            'items': 3
                        },
                        '992': {
                            'items': 4
                        }
                    }
                }">
        <% loop $Collection.Products.limit(6) %>
            <% include ProductCard Product=$Me %>
        <% end_loop %>
    </div>
</section>