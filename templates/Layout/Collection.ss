<main class="main">
    <% if $CustomField('CollectionBanner').Value() %>
        <div class="page-header" style="background-image: url('$CustomField('CollectionBanner').Value().AbsoluteURL');">
    <% else %>
        <div class="page-header" style="background-image: url('$Image.AbsoluteURL');">
    <% end_if %>
        <% if $CustomField('CollectionSubtitle') %>
            <h3 class="page-subtitle">
                $CustomField('CollectionSubtitle')
            </h3>
        <% end_if %>
        <h1 class="page-title">$Title</h1>
        <ul class="breadcrumb">
            <li><a href="/"><i class="d-icon-home"></i></a></li>
            <li class="delimiter">/</li>
            <li>$Title</li>
        </ul>
    </div>
    <div class="page-content mb-10 pb-6">
        <div class="container">
            <div class="row gutter-lg main-content-wrap">
                <aside class="col-lg-3 sidebar sidebar-fixed sidebar-toggle-remain shop-sidebar sticky-sidebar-wrapper">
                    <div class="sidebar-overlay">
                        <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                    </div>
                    <div class="sidebar-content">
                        <form method="get" role="form">
                            <div class="sticky-sidebar" data-sticky-options="{'top': 10}">
                                <div class="filter-actions mb-4">
                                    <a href="#"
                                        class="sidebar-toggle-btn toggle-remain btn btn-outline btn-primary btn-rounded btn-icon-right">Filter<i
                                            class="d-icon-arrow-left"></i></a>
                                    <a href="#" class="filter-clean">Clean All</a>
                                </div>
                                <div class="widget">
                                    <h3 class="widget-title">Search</h3>
                                    <div class="widget-body">
                                        <input class="form-control" type="text" placeholder="Search by Name, SKU,..."
                                               value="$getSearchTerm" name="search" />
                                    </div>
                                </div>
                                <div class="widget widget-collapsible">
                                    <h3 class="widget-title">Price</h3>
                                    <div class="widget-body">
                                        <div class="filter-price-slider" data-min="$getOptions().PriceRange.Min"
                                             data-max="$getOptions().PriceRange.Max" data-step="$getOptions().PriceRange.Step"
                                             data-currency="$SiteSettings.CurrencySymbol()"></div>
                                        <div class="filter-actions">
                                            <div class="filter-price-text">Price:
                                                <span class="filter-price-range"></span>
                                            </div>
                                            <input type="hidden" name="price_min" class="min_val" value="$getOptions().PriceRange.Min" />
                                            <input type="hidden" name="price_max" class="max_val" value="$getOptions().PriceRange.Max" />
                                        </div>
                                    </div>
                                </div>
                                <% loop $getOptions().SearchFilters %>
                                    <div class="widget widget-collapsible">
                                        <h3 class="widget-title">$Name</h3>
                                        <% if $Type == 'color' %>
                                            <div class="row color-swatch">
                                                <% loop $SearchData %>
                                                    <div class="col-1 pr-3">
                                                        <label for="$ID">
                                                            <input type="checkbox" name="search_data[]" value="$ID"
                                                                   id="$ID" />
                                                            <span class="swatch" style="background: #{$Value}"></span>
                                                        </label>
                                                    </div>
                                                <% end_loop %>
                                            </div>
                                        <% else %>
                                            <ul class="widget-body filter-items">
                                                <% loop $SearchData %>
                                                    <li data-value="$ID" class="search-datum <% if $Selected %>active<% end_if %>">
                                                        <a href="#">$Value<span>($Matches)</span></a>
                                                        <input type="checkbox" name="search_data[]" value="$ID" style="display:none"
                                                            id="$ID" <% if $Selected %>checked<% end_if %> />
                                                    </li>
                                                <% end_loop %>
                                            </ul>
                                        <% end_if %>
                                    </div>
                                <% end_loop %>
                                <div class="row pb-2 mt-2">
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-sm btn-primary">Filter</button>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <a href="$AbsoluteLink" class="btn btn-sm btn-outline-secondary">Clear</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </aside>
                <div class="col-lg-9 main-content">
                    <nav class="toolbox sticky-toolbox sticky-content fix-top">
                        <div class="toolbox-left">
                            <a href="#"
                                class="toolbox-item left-sidebar-toggle btn btn-sm btn-outline btn-primary
                                    btn-rounded btn-icon-right d-lg-none">Filter<i
                                    class="d-icon-arrow-right"></i></a>
                            <div class="toolbox-item toolbox-sort select-box text-dark">
                                <label>Sort By :</label>
                                <select name="orderby" class="form-control">
                                    <% loop $Top.getOptions().Sort %>
                                        <option value="$FilterLink" <% if $Selected %>selected="selected"<% end_if %>>
                                            $Option
                                        </option>
                                    <% end_loop %>
                                </select>
                            </div>
                        </div>
                    </nav>
                    <div class="row cols-2 cols-sm-3 product-wrapper">
                        <% loop $PaginatedResults(9) %>
                            <% include ProductCard Product=$Me %>
                        <% end_loop %>
                    </div>
                    <% if PaginatedResults(9).MoreThanOnePage %>
                        <% with PaginatedResults(9) %>
                        <nav class="toolbox toolbox-pagination">
                            <ul class="pagination">
                                <% if $NotFirstPage %>
                                <li class="page-item">
                                    <a class="page-link page-link-prev" href="$PrevLink" aria-label="Previous" tabindex="-1"
                                       aria-disabled="true">
                                        <i class="d-icon-arrow-left"></i>Prev
                                    </a>
                                </li>
                                <% else %>
                                <li class="page-item disabled">
                                    <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1"
                                       aria-disabled="true">
                                        <i class="d-icon-arrow-left"></i>Prev
                                    </a>
                                </li>
                                <% end_if %>
                                <% loop $PaginationSummary %>
                                    <% if $CurrentBool %>
                                        <li class="page-item active" aria-current="page"><a class="page-link" href="#">$PageNum</a>
                                    <% else %>
                                        <% if $Link %>
                                            <li class="page-item"><a class="page-link" href="$Link">$PageNum</a></li>
                                        <% else %>
                                            <li class="page-item">...</li>
                                        <% end_if %>
                                    <% end_if %>
                                <% end_loop %>
                                <% if $NotLastPage %>
                                <li class="page-item">
                                    <a class="page-link page-link-next" href="$NextLink" aria-label="Next">
                                        Next<i class="d-icon-arrow-right"></i>
                                    </a>
                                </li>
                                <% else %>
                                <li class="page-item disabled">
                                    <a class="page-link page-link-next" href="#" aria-label="Next" tabindex="-1" aria-disabled="true">
                                        Next<i class="d-icon-arrow-right"></i>
                                    </a>
                                </li>
                                <% end_if %>
                            </ul>
                        </nav>
                        <% end_with %>
                    <% end_if %>
                </div>
            </div>
        </div>
    </div>
</main>