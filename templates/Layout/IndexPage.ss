<main class="main home">
    <div class="page-content">
        <section class="intro-section">
            <div class="owl-carousel owl-theme row owl-nav-fade intro-slider animation-slider cols-1 gutter-no"
                 data-owl-options="{
                    'nav': true,
                    'dots': false,
                    'loop': false,
                    'items': 1,
                    'autoplay': false,
                    'autoplayTimeout': 8000
                }">
                <% if $Theme.CustomField('BannerCollection1').Value() || $Theme.CustomField('BannerCollection2').Value() || $Theme.CustomField('BannerProduct1').Value() || $Theme.CustomField('BannerProduct2').Value() %>
                    <% if $Theme.CustomField('BannerCollection1').Value() %>
                        <% include BannerCollections Collection=$Theme.CustomField('BannerCollection1').Value() %>
                    <% end_if %>
                    <% if $Theme.CustomField('BannerCollection2').Value() %>
                        <% include BannerCollections Collection=$Theme.CustomField('BannerCollection2').Value() %>
                    <% end_if %>
                    <% if $Theme.CustomField('BannerProduct1').Value() %>
                        <% include BannerProducts Product=$Theme.CustomField('BannerProduct1').Value() %>
                    <% end_if %>
                    <% if $Theme.CustomField('BannerProduct2').Value() %>
                        <% include BannerProducts Product=$Theme.CustomField('BannerProduct2').Value() %>
                    <% end_if %>
                <% else %>
                    <div class="intro-slider banner banner-fixed mb-5" style="background-color: #2d3436">
                        <figure>
                            <div style="background-color: #2d3436; height: 500px;" ></div>
                        </figure>
                        <div class="banner-content x-50 y-50 text-center">
                            <h3 class="banner-title font-secondary text-white text-uppercase slide-animate"
                                data-animation-options="{'name': 'fadeInUp', 'duration': '1.2s', 'delay': '.8s'}">
                                $SiteSettings.Title</h3>
                            <a href="/search" class="btn btn-outline text-primary slide-animate"
                               data-animation-options="{'name': 'fadeInUp', 'duration': '1.2s', 'delay': '1.3s'}">Shop now</a>
                        </div>
                    </div>
                <% end_if %>
            </div>
        </section>

        <% if $Theme.CustomField('CarouselCollection1').Value() || $Theme.CustomField('CarouselCollection2').Value() || $Theme.CustomField('CarouselCollection3').Value() || $Theme.CustomField('CarouselCollection4').Value() %>
            <section class="products-wrapper container pt-2 mt-10 pb-4 mb-4 appear-animate">
                <div class="tab tab-nav-simple tab-nav-center">
                    <ul class="nav nav-tabs" role="tablist">
                        <% if $Theme.CustomField('CarouselCollection1').Value() %>
                            <li class="nav-item">
                                <a class="nav-link active" href="#$Theme.CustomField('CarouselCollection1').Value().ID">$Theme.CustomField('CarouselCollection1').Value().Title</a>
                            </li>
                        <% end_if %>
                        <% if $Theme.CustomField('CarouselCollection2').Value() %>
                            <li class="nav-item">
                                <a class="nav-link" href="#$Theme.CustomField('CarouselCollection2').Value().ID">$Theme.CustomField('CarouselCollection2').Value().Title</a>
                            </li>
                        <% end_if %>
                        <% if $Theme.CustomField('CarouselCollection3').Value() %>
                            <li class="nav-item">
                                <a class="nav-link" href="#$Theme.CustomField('CarouselCollection3').Value().ID">$Theme.CustomField('CarouselCollection3').Value().Title</a>
                            </li>
                        <% end_if %>
                        <% if $Theme.CustomField('CarouselCollection4').Value() %>
                            <li class="nav-item">
                                <a class="nav-link" href="#$Theme.CustomField('CarouselCollection4').Value().ID">$Theme.CustomField('CarouselCollection4').Value().Title</a>
                            </li>
                        <% end_if %>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="$Theme.CustomField('CarouselCollection1').Value().ID">
                            <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'items': 4,
                                    'nav': false,
                                    'dots': false,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        }
                                    }
                                }">
                                    <% loop $Theme.CustomField('CarouselCollection1').Value.Products.limit(4) %>
                                        <% include ProductCard Product=$Me %>
                                    <% end_loop %>
                            </div>
                        </div>
                        <div class="tab-pane" id="$Theme.CustomField('CarouselCollection2').Value().ID">
                            <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'items': 4,
                                    'nav': false,
                                    'dots': false,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        }
                                    }
                                }">
                                <% loop $Theme.CustomField('CarouselCollection2').Value().Products.limit(4) %>
                                    <% include ProductCard Product=$Me %>
                                <% end_loop %>
                            </div>
                        </div>
                        <div class="tab-pane" id="$Theme.CustomField('CarouselCollection3').Value().ID">
                            <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'items': 4,
                                    'nav': false,
                                    'dots': false,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        }
                                    }
                                }">
                                <% loop $Theme.CustomField('CarouselCollection3').Value().Products.limit(4) %>
                                    <% include ProductCard Product=$Me %>
                                <% end_loop %>
                            </div>
                        </div>
                        <div class="tab-pane" id="$Theme.CustomField('CarouselCollection4').Value().ID">
                            <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'items': 4,
                                    'nav': false,
                                    'dots': false,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        }
                                    }
                                }">
                                <% loop $Theme.CustomField('CarouselCollection4').Value.Products.limit(4) %>
                                    <% include ProductCard Product=$Me %>
                                <% end_loop %>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <% end_if %>

        <% if $Theme.CustomField('FeaturedCollection1') || $Theme.CustomField('FeaturedCollection2') || $Theme.CustomField('FeaturedCollection3') || Integration('MailChimp') %>
            <section class="banners-section">
                <div class="container">
                    <div class="row">
                    <% if $Theme.CustomField('FeaturedCollection1') || $Theme.CustomField('FeaturedCollection2') %>
                        <% if $Theme.CustomField('FeaturedCollection1').Value() %>
                            <% with $Theme.CustomField('FeaturedCollection1').Value %>
                                <div class="<% if $Theme.CustomField('FeaturedCollection2').Value() %> col-md-7 <% else %> col-12 text-center <% end_if %>">
                                    <div class="banner banner1 banner-fixed overlay-zoom mb-4"
                                         style="background-color: #F7F7F7; max-height: 305px;">
                                        <figure>
                                            <img src="$Image.Fill(680,305).Link" alt="banner" width="680"
                                                 height="305" />
                                        </figure>
                                        <div class="banner-content y-50">
                                            <% if $CustomField('CollectionSubtitle').Value() %>
                                                <h4 class="banner-subtitle">$CustomField('CollectionSubtitle').Value()</h4>
                                            <% end_if %>
                                            <h3 class="banner-title text-uppercase font-weight-bold mb-6">$Title</h3>
                                            <a href="$AbsoluteLink"
                                               class="btn btn-sm btn-rounded btn-outline btn-primary">Shop now</a>
                                        </div>
                                    </div>
                                </div>
                            <% end_with %>
                        <% end_if %>
                        <% if $Theme.CustomField('FeaturedCollection2').Value() %>
                            <% with $Theme.CustomField('FeaturedCollection2').Value %>
                                <div class=" <% if $Theme.CustomField('FeaturedCollection1').Value() %> col-md-5 <% else %> col-12 text-center <% end_if %>">
                                    <div class="banner banner2 banner-fixed overlay-zoom mb-4" style="background-color: #EAEBF0; max-height: 305px;">
                                        <figure>
                                            <img src="$Image.Fill(480,305).Link" alt="banner" width="480" height="305" />
                                        </figure>
                                        <div class="banner-content y-50">
                                            <% if $CustomField('CollectionSubtitle').Value() %>
                                                <h4 class="banner-subtitle">$CustomField('CollectionSubtitle').Value()</h4>
                                            <% end_if %>
                                            <h3 class="banner-title text-uppercase font-weight-bold mb-6">$Title</h3>
                                            <a href="$AbsoluteLink" class="btn btn-sm btn-rounded btn-dark">Shop now</a>
                                        </div>
                                    </div>
                                </div>
                            <% end_with %>
                        <% end_if %>
                    <% end_if %>
                    <% if $Theme.CustomField('FeaturedCollection3') || Integration('Mailchimp') %>
                        <% if Integration('Mailchimp') %>
                            <div class="<% if $Theme.CustomField('FeaturedCollection3').Value() %> col-md-5 <% else %> col-12 <% end_if %>">
                                <div class="banner banner3 banner-fixed mb-4">
                                    <div class="banner-content x-50 y-50 text-center w-100 pr-2 pl-2 appear-animate">
                                        <h3 class="banner-title text-uppercase mb-0">
                                            Subscribe to our<strong class="d-block">Newsletter</strong></h3>

                                        <div class="alert alert-round col-9 mt-5 mx-auto mb-2" id="subscribe-alert" style="display:none;"></div>
                                        <form method="get" class="input-wrapper mailchimp-subscribe" data-alert="#subscribe-alert">
                                            <input type="email" class="form-control text-center" name="email" id="email"
                                                    placeholder="Email address here..." required/>
                                            <button class="btn btn-dark btn-md" type="submit">Subscribe<i
                                                    class="d-icon-arrow-right"> </i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        <% end_if %>
                        <% if $Theme.CustomField('FeaturedCollection3').Value() %>
                            <% with $Theme.CustomField('FeaturedCollection3').Value %>
                            <div class="<% if Integration('Mailchimp') %> col-md-7 <% else %> col-12 text-center <% end_if %>">
                                <div class="banner banner4 banner-fixed overlay-zoom mb-4"
                                     style="background-color: #E5EDEF; max-height: 305px;">
                                    <figure>
                                        <img src="$Image.Fill(680,305).Link" alt="banner" width="680"
                                             height="305"/>
                                    </figure>
                                    <div class="banner-content y-50">
                                        <h4 class="banner-subtitle">$CustomField('CollectionSubtitle').Value()</h4>
                                        <h3 class="banner-title text-uppercase font-weight-bold mb-6">$Title</h3>
                                        <a href="$AbsoluteLink"
                                           class="btn btn-sm btn-rounded btn-outline btn-primary">Shop now</a>
                                    </div>
                                </div>
                            </div>
                            <% end_with %>
                        <% end_if %>
                    <% end_if %>
                    </div>
                </div>
            </section>
        <% end_if %>
        <% if $Theme.CustomField('DisplayCollection1').Value() %>
            <% include DisplayCollection Collection=$Theme.CustomField('DisplayCollection1').Value() %>
        <% end_if %>

        <% if $Theme.CustomField('ParallaxCollection').Value() %>
            <% include ParallaxCollection Collection=$Theme.CustomField('ParallaxCollection').Value() %>
        <% end_if %>

        <% if $Theme.CustomField('DisplayCollection2').Value() %>
            <% include DisplayCollection Collection=$Theme.CustomField('DisplayCollection2').Value() %>
        <% end_if %>
    </div>
</main>