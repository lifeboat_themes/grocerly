<main class="main cart">
    <div class="page-content pt-10 pb-10">
        <div class="container mt-8 mb-8">
            <div class="row gutter-lg">
                <div class="col">
                    <h1>$Title</h1>
                    $Content
                </div>
            </div>
        </div>
    </div>
</main>